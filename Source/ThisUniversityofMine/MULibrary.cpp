// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MULibrary.h"
#include "MUCameraController.h"
#include "MUBuildingGhost.h"

void UMULibrary::SetMouseWorldPosition(AMUCameraController * controller, float distance)
{
	if (controller)
	{
		FVector WorldLocation;
		FVector WorldDirection;
		controller->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
		FHitResult Hit;
		controller->GetWorld()->LineTraceSingleByChannel(Hit, WorldLocation, WorldDirection*distance + WorldLocation, ECC_GameTraceChannel1);
		if (Hit.bBlockingHit)
		{
			controller->GetGhost()->SetMousePosition(Hit.Location);
		}
	}
}

int32 UMULibrary::SetGridSnap(float axis, int32 size)
{
	return FMath::FloorToInt((size / 2 + axis) / size) * size;
}
