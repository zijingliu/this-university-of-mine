// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MULibrary.generated.h"

/**
 * 
 */
class AMUCameraController;
UCLASS()
class THISUNIVERSITYOFMINE_API UMULibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "MULibrary")
	static void SetMouseWorldPosition(AMUCameraController* controller, float distance);	

	UFUNCTION(BlueprintCallable, Category = "MULibrary")
	static int32 SetGridSnap(float axis, int32 size);
};
