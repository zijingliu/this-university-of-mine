// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MUPawnWithCamera.h"
#include "MUUniversity.h"
#include "MUCameraController.generated.h"

/**
 * 
 */
class AMUBuildingGhost;
class AMUAIManager;
class AMUBuilding;
UCLASS()
class THISUNIVERSITYOFMINE_API AMUCameraController : public APlayerController
{
	GENERATED_BODY()
public:
    
    // Tutorial texts list
    FString Tutorial = TEXT("DSAD");
    
	void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
	void SetupInputComponent() override;
	void SetBuildMode(bool value);
    bool EventBuild(AMUBuilding* building);
    
    void DisplayInformation(FString text, int32 color);
    void DisplayDialog(int32 index, int32 next);
    void CloseDialog();

    
    void UpdateBalance(int32 change);
    void UpdateReputation(int32 change);
    
    void UpdateBalance(int32 type, int32 change);
    void UpdateReputation(int32 type, int32 change);
    void UpdateUIDate(int32 index, int32 change);
    
    void UpdateMonth(int32 change);
    void UpdateSemester(int32 change);
    
    //UFUNCTION(BlueprintCallable, Category = "UMG Game")
    void ExtraAdmission(int32 type);
    
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    float GetRemainingTime();
    
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    AMUBuildingGhost* GetGhost();
    
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    void StartGame();

	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void ApplyPolicy(bool type);

	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void TryHire(int32 type);
	
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void ProcessDialog();


protected:
	UPROPERTY(EditAnywhere, Category = Sensitivity)
	float PanSensitivity = 5.0f;

	UPROPERTY(EditAnywhere, Category = Sensitivity)
	float ZoomSensitivity = 50.0f;

	UPROPERTY(EditAnywhere, Category = Sensitivity)
	float EdgeSensitivity = 10.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="AI")
    TSubclassOf<AMUAIManager> AIManager;

	UPROPERTY(EditDefaultsOnly, Category = Sound) 
	class USoundCue* BuildSoundCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* BackgroundCue;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	TArray<class USoundCue*> TutorialSoundCue;

	UPROPERTY(Transient) 
	class UAudioComponent* SoundComponent;
    
	float SightDistance = 1000.0f;

	AMUPawnWithCamera* CameraPawn;
	bool bEnablePan = false;
	bool bEnableMoveX = true;
	bool bEnableMoveY = true;
	bool bBuildMode = false;
    
    bool bAdmBuild = false;
	bool bHousingBuild = false;
    bool bEngBuild = false;
	bool bArtScienceBuild = false;
	bool bBusinessBuild = false;
    
	AMUBuildingGhost* myBuildingGhost;
    AMUAIManager* myAIManager;

	float MovementSpeedCalculation();
	float SetDistance();
	FVector UpdateMousePosition();
	void PanX(float value);
	void PanY(float value);
	void EnablePan() { bEnablePan = true; }
	void DisablePan() { bEnablePan = false; }
	void Clicked();
    void RightClicked();
	void ZoomIn();
	void ZoomOut();
    void RotateGhostClockWise();
    void RotateGhostCounterClockWise();
	void PlayBuild();

	int32 progression = 0;
	TArray<int32> nextList;
};

