// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MUCameraController.h"
#include "MUBuilding.generated.h"

UCLASS()
class THISUNIVERSITYOFMINE_API AMUBuilding : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMUBuilding();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetController(AMUCameraController* C);
    
    UStaticMeshComponent* GetStaticMeshComp() { return BuildingMeshCompoenent;}

	UStaticMesh* GetBuildingMesh();

	FVector GetBuildingScale();
    
    void SetType(int32 value);
    int32 GetType() { return type;}
    
    void SetCost(float value);
    float GetCost() { return cost;}
    
    void SetSatChange(float value);
    float GetSatChange() { return satChange;}
    
    void SetBuildingInfor(FString text);
    FString GetBuildingInfor() { return BuildInfor;}
    
    float GetAcademicChange() { return academicChange; }

    float GetReputationChange() { return reputationChange; }

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
    UStaticMeshComponent* BuildingMeshCompoenent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
    UStaticMesh* BuildingMesh;	

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    int32 type;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    float cost;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    float satChange;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    float academicChange;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    float reputationChange;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildingProperty)
    FString BuildInfor;
    
    AMUCameraController* controller;
};
