// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUBuilding.h"


// Sets default values
AMUBuilding::AMUBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	BuildingMeshCompoenent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BuildingMeshCompoenent"));
	BuildingMeshCompoenent->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void AMUBuilding::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMUBuilding::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AMUBuilding::SetController(AMUCameraController * C)
{
	controller = C;
}

UStaticMesh* AMUBuilding::GetBuildingMesh()
{
	return BuildingMesh;
}

FVector AMUBuilding::GetBuildingScale()
{
	return BuildingMeshCompoenent->GetComponentScale();
}


