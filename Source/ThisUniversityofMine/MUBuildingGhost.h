// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MUCameraController.h"
#include "MUBuildingGhost.generated.h"

UCLASS()
class THISUNIVERSITYOFMINE_API AMUBuildingGhost : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMUBuildingGhost();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    void SetController(AMUCameraController* parent);
    
    void OnSpawn(UStaticMesh* StaticMesh);
    void ClearSpawn();

	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void EventBuild(TSubclassOf<class AMUBuilding> MainBuilding);

	void SetMousePosition(FVector pos);
    void RotateGhost(float sign);
	void Clicked();
	void SetNormalMaterial(UMaterialInstance* n);
	void SetErrorMaterial(UMaterialInstance* e);

protected:
    AMUCameraController* controller;
    bool IsActive = false;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
    UStaticMeshComponent* BuildingMeshCompoent;
    UStaticMesh* BuildingMesh;
	TSubclassOf<class AMUBuilding> CurrentBuilding;
	FVector MousePosition;
	int32 GridSize = 50;
	bool IsOverlapping = false;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	UMaterialInstance* Error;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	UMaterialInstance* Normal;

	FVector SetGridSnap();
	bool CheckOverlap();

    
private:
    AMUBuilding* Building;
};
