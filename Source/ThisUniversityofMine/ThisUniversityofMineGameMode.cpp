// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "ThisUniversityofMineGameMode.h"
#include "MUPawnWithCamera.h"
#include "MUCameraController.h"
#include "MUBuildingGhost.h"

AThisUniversityofMineGameMode::AThisUniversityofMineGameMode(){
	PlayerControllerClass = AMUCameraController::StaticClass();
	DefaultPawnClass = AMUPawnWithCamera::StaticClass();
    
}

void AThisUniversityofMineGameMode::BeginPlay()
{
	Super::BeginPlay();
	//ChangeMenuWidget(StartingWidgetClass);
	if (GetWorld()->GetFirstPlayerController())
	{
		controller = Cast<AMUCameraController>(GetWorld()->GetFirstPlayerController());
	}
	controller->GetGhost()->SetNormalMaterial(Normal);
	controller->GetGhost()->SetErrorMaterial(Error);
}

void AThisUniversityofMineGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

AMUCameraController * AThisUniversityofMineGameMode::GetController()
{
	return controller;
}
