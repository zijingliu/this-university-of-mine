// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUAIController.h"
#include "MUAICharacter.h"
#include <iostream>
#include <cmath>
#include "MUAIManager.h"

void AMUAIController::BeginPlay(){
    Super::BeginPlay();
//    Destination = FVector(FMath::RandRange(-400, 400), FMath::RandRange(-400, 400), 0);
//    MoveToLocation(Destination);
    
    autoControl = true;
    hasEvent = false;
}

void AMUAIController::Tick(float DeltaTime){
    Super::Tick(DeltaTime);
//    if (autoControl) {
//        auto pawn = GetPawn();
//        auto location = pawn->GetActorLocation();
//        float dist = FVector::Dist(location, Destination);
//        if (dist < 150.0f){
//            Destination = FVector(FMath::RandRange(-1 * AMUAIManager::bound, AMUAIManager::bound), FMath::RandRange(-1 * AMUAIManager::bound, AMUAIManager::bound), 0);
//            MoveToLocation(Destination);
//        }
//    }
}

void AMUAIController::allowAutoControl(){
    autoControl = true;
}

void AMUAIController::goTo(FVector destination, bool event){
    if ((Destination == destination || !autoControl) && !event) return;
    if (hasEvent) return;
    if (destination != FVector(0, 0, 0)) autoControl = false;
    Destination = destination;
    MoveToLocation(Destination);
    GetPawn()->SetActorHiddenInGame(false);
    hasEvent = event;
}

void AMUAIController::arriveDest(){
    StopMovement();
    if (!hasEvent) {
        GetPawn()->SetActorHiddenInGame(true);
//        allowAutoControl();
        FTimerHandle handle;
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIController::allowAutoControl, 3.0f);
    }
}