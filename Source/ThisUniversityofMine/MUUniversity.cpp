// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUUniversity.h"
#include "MUDepartment.h"
#include "MUAIManager.h"
#include <iostream>

MUUniversity::MUUniversity()
{
    balance = 1000000;
    tuition = 5000;
    security = 30;
    environment = 0;
    maxPopulation = 0;
    salary = 50000;
}

void MUUniversity::InitilizeUniversity()
{
    myManager->GetController()->UpdateUIDate(0, (int32)getAcademic());
    myManager->GetController()->UpdateUIDate(1, (int32)getSatisfaction());
    myManager->GetController()->UpdateUIDate(2, (int32)getPopulation(-1));
    myManager->GetController()->UpdateUIDate(3, (int32)getMaxPopulation());
    myManager->GetController()->UpdateUIDate(4, (int32)getInstructors());
    myManager->GetController()->UpdateUIDate(5, (int32)getReputation());
    myManager->GetController()->UpdateUIDate(6, 0);
    myManager->GetController()->UpdateUIDate(7, 0);
    myManager->GetController()->UpdateUIDate(8, 0);
    myManager->GetController()->UpdateUIDate(9, 0);
    myManager->GetController()->UpdateUIDate(10, 0);
    myManager->GetController()->UpdateUIDate(11, 0);
    myManager->GetController()->UpdateUIDate(12, 0);
    myManager->GetController()->UpdateUIDate(13, 0);
    myManager->GetController()->UpdateUIDate(14, 0);
    myManager->GetController()->UpdateUIDate(15, 0);
    myManager->GetController()->UpdateUIDate(16, 0);
    myManager->GetController()->UpdateUIDate(17, 0);
    myManager->GetController()->UpdateUIDate(18, 0);
    myManager->GetController()->UpdateUIDate(19, 0);
    myManager->GetController()->UpdateUIDate(20, 0);
    myManager->GetController()->UpdateUIDate(21, 0);
    myManager->GetController()->UpdateUIDate(22, 0);
    myManager->GetController()->UpdateUIDate(23, 0);
}

void MUUniversity::TechPolicy()
{
	mDepartments[1]->AcdChange(0.3f);
	SatChange(-0.1f);
}

void MUUniversity::PartyPolicy()
{
	mDepartments[2]->SatChange(0.3f);
	for (auto& iter : mDepartments) {
		iter.second->AcdChange(-0.1f);
	}
}

bool MUUniversity::TryHire(int32 type)
{
	if (mDepartments[type]->hire(1))
	{
		return true;
	}
	else
	{
		return false;
	}
}


int MUUniversity::getPopulation(int grade){
    int population = 0;
    for (auto& iter : mDepartments){
        population += iter.second->getPopulation(grade);
    }
    return population;
}

int MUUniversity::getInstructors(){
    int instructors = 0;
    for (auto& iter : mDepartments){
        instructors += iter.second->getInstructors();
    }
    return instructors;
}

float MUUniversity::getTotalScore(){
    int sum = sqrt(getReputation() * getAcademic());
    sum = sqrt(sum * getSecurity());
    return sqrt(sum * getEnvironment());
}

float MUUniversity::getReputation(){
    float reputation = 0;
    for (auto& iter : mDepartments){
        reputation += iter.second->getReputation();
    }
    return reputation;
}

float MUUniversity::getAcademic(){
    float acd = 0;
    for (auto& iter : mDepartments){
        acd += iter.second->getAcademic();
    }
    return acd;
}

float MUUniversity::getSatisfaction(){
    int sat = 0;
    for (auto& iter : mDepartments){
        sat += iter.second->getSatisfation();
    }
    return (mDepartments.size() == 0)? 0 : sat / mDepartments.size();
}

void MUUniversity::newSemester(){
    FString summary = FString();
    for (auto& iter : mDepartments){
        summary.Empty();
        if(iter.first == ArtScience)
        {
            summary += FString::Printf(TEXT(" New semester Art&Science Department:"));
        }
        else if(iter.first == Engineering)
        {
            summary += FString::Printf(TEXT(" New semester Engineering Department:"));
        }
        else if(iter.first == Business)
        {
            summary += FString::Printf(TEXT(" New semester Business Department:"));
        }
        iter.second->newSemester(summary);
        myManager->DisplayMsg(summary, 2);
        float preBalance = balance;
        balance += iter.second->getPopulation(-1) * tuition;
        balance -= iter.second->getInstructors() * salary;
        int32 balanceChange = (int32)(balance - preBalance);
        myManager->GetController()->UpdateBalance(iter.second->type, balanceChange);
    }
    summary.Empty();
    summary += FString(TEXT("New semester addmission period is over!"));
    myManager->DisplayMsg(summary, 2);
    myManager->GetController()->UpdateBalance((int32)balance);
    myManager->GetController()->UpdateReputation((int32)getReputation());
    
    myManager->GetController()->UpdateUIDate(0, (int32)getAcademic());
    myManager->GetController()->UpdateUIDate(2, (int32)getPopulation(-1));
    myManager->GetController()->UpdateUIDate(5, (int32)getReputation());
    
    myManager->AddAI();
}

int MUUniversity::getAPosition(){
    return maxPopulation - (getPopulation(grades) - getPopulation(grades-1));
}

void MUUniversity::ChangeBalance(float change)
{
	balance += change;
	myManager->GetController()->UpdateBalance((int32)balance);
}

bool MUUniversity::CheckBalance(float change)
{
	return balance >= change;
}

bool MUUniversity::build(int32 type, const FVector loc, float cost, float satChange, float acdChange, float repuChange){
    if (cost > balance) return false;
    balance -= cost;
    myManager->GetController()->UpdateBalance((int32)balance);
    if (type < 3)
    {
        if (mDepartments[type]) {
            mDepartments[type]->build();
            mDepartments[type]->SatChange(satChange);
        }
        else {
            mDepartments[type] = new MUDepartment(this, type);
            
            myManager->GetController()->UpdateUIDate(0, (int32)getAcademic());
            myManager->GetController()->UpdateUIDate(1, (int32)getSatisfaction());
            myManager->GetController()->UpdateUIDate(4, (int32)getInstructors());
            myManager->GetController()->UpdateUIDate(5, (int32)getReputation());
            myManager->GetController()->UpdateReputation((int32)getReputation());
        }
        
        myManager->GetController()->UpdateBalance(type, (int32)-cost);
        if (type == Engineering){
            GetManager()->buildingLocations[1] = loc;
            GetManager()->CelebrateEng();
        }
    }
    if (type < 10)
    {
        if(type == Housing)
        {
            maxPopulation += 200;
            myManager->GetController()->UpdateUIDate(3, (int32)getMaxPopulation());
        }
        else if(type == Lab)
        {
            mDepartments[1]->SatChange(satChange);
            mDepartments[1]->AcdChange(acdChange);
            mDepartments[1]->repChange(repuChange);
        }
        allLocs.push_back(loc);
        if (type == HealthCenter){
            GetManager()->buildingLocations[7] = loc;
            GetManager()->HealthCenter();
        }
    }
    return true;
}


void MUUniversity::ExtraAdmission(int32 type)
{
    //auto dep = mDepartments[type];
    //if(dep != NULL)
    //{
    //    FString summary;
    //    if(type == 0)
    //    {
    //        summary = FString(TEXT("Students transfer in Art&Science! "));
    //    }
    //    else if(type == 1)
    //    {
    //        summary = FString(TEXT("Students transfer in Engineering! "));
    //    }
    //    if(type == 2)
    //    {
    //        summary = FString(TEXT("Students transfer in Business! "));
    //    }
    //    dep->ExtraAdmission(summary);
    //    myManager->GetController()->DisplayInformation(summary);
    //    myManager->AddAI();
    //}
}

bool MUUniversity::hire(int num){
    if (balance >= num * salary) {
        balance -= num * salary;
        myManager->GetController()->UpdateUIDate(4, (int32)(getInstructors()+num));
		myManager->GetController()->UpdateBalance((int32)balance);
        return true;
    }
    else {
        return false;
    }
}

void MUUniversity::SatChange(float change){
    for (auto& iter : mDepartments){
        iter.second->SatChange(change);
    }
}

const FVector MUUniversity::getRandLoc(){
    if (allLocs.empty()) {
        return FVector(0,0,0);
    }
    else {
        int index = abs(rand())%allLocs.size();
//        std::cout << "index " << index << std::endl;
        return allLocs[index];
    }
}
