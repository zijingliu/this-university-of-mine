// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "MUPawnWithCamera.generated.h"

UCLASS()
class THISUNIVERSITYOFMINE_API AMUPawnWithCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMUPawnWithCamera();

	USpringArmComponent* GetCameraSpring() { return OurCameraSpringArm; }
    
protected:
    USpringArmComponent* OurCameraSpringArm;
    UCameraComponent* OurCamera;
};
