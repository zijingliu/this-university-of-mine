// Fill out your copyright notice in the Description page of Project Settings.
#include <map>
#include <vector>

#pragma once
/**
 * 
 */

class AMUAIManager;
class THISUNIVERSITYOFMINE_API MUUniversity
{
public:
	MUUniversity();
	~MUUniversity();
    
    static const int32 ArtScience = 0;
    static const int32 Engineering = 1;
    static const int32 Business = 2;
    static const int32 Admission = 3;
    static const int32 Lab = 4;
    static const int32 Dinning = 5;
    static const int32 Housing = 6;
    static const int32 HealthCenter = 7;
    static const int32 Shop = 8;
    static const int32 Stadium = 9;
    static const int32 Bench = 10;
    static const int32 Billboard = 11;
    static const int32 GarbageBin = 12;
    static const int32 Light = 13;
    static const int32 Tree = 14;
    
    float getTotalScore();
    
    float getReputation();
    
    int32 getPopulation(int32 grade);
    
    int getInstructors();
    
    float getBalance(){ return balance; }
    float getTuition(){ return tuition; }
    float getSecurity(){ return security; }
    float getEnvironment(){ return environment; }
    float getAcademic();
    int getMaxPopulation(){ return maxPopulation; }
    float getSatisfaction();
    void newSemester();
    int getAPosition();
	void ChangeBalance(float change); 
	bool CheckBalance(float change); 
	float getSalary() { return salary; }
    AMUAIManager* GetManager() { return myManager; }
    
    bool build(int32 type, const FVector loc, float cost, float satChange = 0.0f, float acdChange = 0.0f, float repuChange = 0.0f);
    
    bool hire(int num);
    
    const FVector getRandLoc();
    
    void SetAIManger(AMUAIManager* owner) { myManager = owner; }
    void SatChange(float change);
    
    void ExtraAdmission(int32 type);
    
    void InitilizeUniversity();

	void TechPolicy();
	void PartyPolicy();

	// Hirement enforcement
	bool TryHire(int32 type);
    
private:
    std::vector<FVector> allLocs;
    static const int grades = 4;
    std::map<int32,class MUDepartment*> mDepartments;
    float salary;
    float balance;
    float tuition;
    float security;
    float environment;
    int maxPopulation;

    AMUAIManager* myManager;
};
