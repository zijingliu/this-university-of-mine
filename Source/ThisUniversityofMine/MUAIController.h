// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "MUAIController.generated.h"

/**
 * 
 */
UCLASS()
class THISUNIVERSITYOFMINE_API AMUAIController : public AAIController
{
	GENERATED_BODY()
public:
    void Tick(float DeltaTime) override;
    void BeginPlay() override;
    void goTo(FVector destination, bool event = false);
    bool isBusy(){ return !autoControl; }
    const FVector& getDest(){ return Destination; }
    void arriveDest();
    void allowAutoControl();
    bool inEvent(){ return hasEvent; }
    void endEvent() { hasEvent = false; }
private:
    bool hasEvent;
    bool autoControl;
    FVector Destination;
};
