// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUBuildingGhost.h"
#include "MUBuilding.h"
#include "MUCameraController.h"
#include "MULibrary.h"
#include "MUUniversity.h"
#include "MUAIManager.h"


void AMUCameraController::BeginPlay()
{
	Super::BeginPlay();
	if (GetPawn())
	{
		CameraPawn = Cast<AMUPawnWithCamera>(GetPawn());
	}
	else
	{
		CameraPawn = nullptr;
	}

	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;

	myBuildingGhost = GetWorld()->SpawnActor<AMUBuildingGhost>(AMUBuildingGhost::StaticClass(), FVector(0.f, 0.f, 0.f), FRotator(0.f,0.f,0.f));
	myBuildingGhost->SetController(this);
    
    myAIManager = GetWorld()->SpawnActor<AMUAIManager>(AIManager, FVector(0.f, 0.f, 0.f), FRotator(0.f,0.f,0.f));
    
    myAIManager->SetController(this);

	int32 tableinit[] = {1,1,1,1,0,0,0,1,1,1,1,1,2};
	nextList.Append(tableinit, ARRAY_COUNT(tableinit));

	SoundComponent = NULL;
}

void AMUCameraController::UpdateUIDate(int32 index, int32 change)
{
    const FString command = FString::Printf(TEXT("UpdateUniversityOverview %d %d"), index, change);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

void AMUCameraController::UpdateBalance(int32 type, int32 change)
{
    if(type == 0)
    {
        const FString command = FString::Printf(TEXT("SetASBudgetChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
    else if(type == 1)
    {
        const FString command = FString::Printf(TEXT("SetEBudgetChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
    else if(type == 2)
    {
        const FString command = FString::Printf(TEXT("SetBBudgetChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
}

void AMUCameraController::UpdateReputation(int32 type, int32 change)
{
    if(type == 0)
    {
        const FString command = FString::Printf(TEXT("SetASReputationChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
    else if(type == 1)
    {
        const FString command = FString::Printf(TEXT("SetEReputationChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
    else if(type == 2)
    {
        const FString command = FString::Printf(TEXT("SetBReputationChange %d"), change);
        FOutputDeviceDebug debug;
        GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
    }
}

void AMUCameraController::Tick(float DeltaSeconds)
{
	if (bBuildMode && !bEnablePan)
	{
		if (myBuildingGhost)
		{
			UMULibrary::SetMouseWorldPosition(this, SetDistance());
		}
	}
}

void AMUCameraController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("Pan", IE_Pressed, this, &AMUCameraController::EnablePan);
	InputComponent->BindAction("Pan", IE_Released, this, &AMUCameraController::DisablePan);
	InputComponent->BindAxis("MouseX", this, &AMUCameraController::PanX);
	InputComponent->BindAxis("MouseY", this, &AMUCameraController::PanY);

	InputComponent->BindAction("OnClick", IE_Pressed, this, &AMUCameraController::Clicked);
    InputComponent->BindAction("RightClick", IE_Pressed, this, &AMUCameraController::RightClicked);
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AMUCameraController::ZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &AMUCameraController::ZoomOut);
    InputComponent->BindAction("RotateClockWise", IE_Pressed, this, &AMUCameraController::RotateGhostClockWise);
    InputComponent->BindAction("RotateCounterClockWise", IE_Pressed, this, &AMUCameraController::RotateGhostCounterClockWise);
}

void AMUCameraController::RotateGhostClockWise()
{
    myBuildingGhost->RotateGhost(1.0f);
}

void AMUCameraController::RotateGhostCounterClockWise()
{
    myBuildingGhost->RotateGhost(-1.0f);
}

void AMUCameraController::PlayBuild()
{  
	auto AC = UGameplayStatics::SpawnSoundAttached(BuildSoundCue, RootComponent);
}

AMUBuildingGhost * AMUCameraController::GetGhost()
{
	return myBuildingGhost;
}

void AMUCameraController::SetBuildMode(bool value)
{
	bBuildMode = value;
}

void AMUCameraController::ExtraAdmission(int32 type)
{
    myAIManager->getUniversity()->ExtraAdmission(type);
}

bool AMUCameraController::EventBuild(AMUBuilding* building)
{
    if(!bAdmBuild && building->GetType() != MUUniversity::Admission)
    {
        DisplayInformation(FString(TEXT("You university need a admission office!!")), 0);
        return false;
    }
	else if (bAdmBuild && !bHousingBuild && building->GetType() != MUUniversity::Housing)
	{
		DisplayInformation(FString(TEXT("You need to build your housing building first!")), 0);
		return false;
	}
    else if(!bEngBuild && building->GetType() == MUUniversity::Lab)
    {
        DisplayInformation(FString(TEXT("You need to build you engineering school before building a lab building!")), 0);
        return false;
    }

    if(myAIManager->EventBuild(building, myBuildingGhost->GetActorLocation()))
    {

        if(!bAdmBuild)
        {
            bAdmBuild = true;
			progression = 5;
			DisplayDialog(progression, nextList[progression]);
        }
		else if (!bHousingBuild)
		{
			bHousingBuild = true;
			progression = 6;
			DisplayDialog(progression, nextList[progression]);
		}
        else if(!bEngBuild && building->GetType() == MUUniversity::Engineering)
        {
            bEngBuild = true;
			if (progression == 6)
			{
				progression++;
				DisplayDialog(progression, nextList[progression]);
			}
        }
		else if (!bArtScienceBuild && building->GetType() == MUUniversity::ArtScience)
		{
			bArtScienceBuild = true;
			if (progression == 6)
			{
				progression++;
				DisplayDialog(progression, nextList[progression]);
			}
		}
		else if (!bBusinessBuild && building->GetType() == MUUniversity::Business)
		{
			bBusinessBuild = true;
			if (progression == 6)
			{
				progression++;
				DisplayDialog(progression, nextList[progression]);
			}
		}
        
        FString buildResult = building->GetBuildingInfor();
        buildResult += FString::Printf(TEXT(" Cost is %d k"), (int32)(building->GetCost()/1000.f));
        DisplayInformation(buildResult, 1);
        
		PlayBuild();
        return true;
    }
    else
    {
		DisplayInformation(FString(TEXT("Budget Not Enough!!!")), 0);
        return false;
    }
}

float AMUCameraController::GetRemainingTime()
{
    return myAIManager->GetRemainingTime();
}

void AMUCameraController::StartGame()
{
    myAIManager->StartGame();
	auto AC = UGameplayStatics::SpawnSoundAttached(BackgroundCue, RootComponent);

}

void AMUCameraController::ApplyPolicy(bool type)
{
	if (type)
	{
		if (myAIManager->ApplyPolicy(AMUAIManager::PolicyTechSchool))
		{
			DisplayInformation(FString(TEXT("Tech School Policy Applied! Cost 1000K!")), 1);
		}
		else
		{
			DisplayInformation(FString(TEXT("Not Enough Budget to apply the policy!")), 0);
		}
	}
	else
	{
		if (myAIManager->ApplyPolicy(AMUAIManager::PolicyPartyScool))
		{
			DisplayInformation(FString(TEXT("Party School Policy Applied! Cost 1000K!")), 1);
		}
		else
		{
			DisplayInformation(FString(TEXT("Not Enough Budget to apply the policy!")), 0);
		}
	}
}

void AMUCameraController::TryHire(int32 type)
{
	auto text = FString(TEXT("Successfully hire an instructor for "));
	if (type == MUUniversity::ArtScience && bArtScienceBuild)
	{
		text += FString(TEXT("Art and Science school!"));
	}
	else if (type == MUUniversity::Engineering && bEngBuild)
	{
		text += FString(TEXT("Engineering school!"));
	}
	else if (type == MUUniversity::Business && bBusinessBuild)
	{
		text += FString(TEXT("Business school!"));
	}
	else
	{
		DisplayInformation(FString(TEXT("Not cheating! You don't even have a department building for the school!")), 0);
		return;
	}

	if (myAIManager->getUniversity()->TryHire(type))
	{

		DisplayInformation(text, 1);
	}
	else
	{
		DisplayInformation(FString(TEXT("Not enough budget to hire more instructor!")), 0);
	}
}

void AMUCameraController::DisplayInformation(FString text, int32 color)
{
	text += FString::Printf(TEXT("|%d"), color);
	const FString function = FString::Printf(TEXT("SetMessage %s"), *text);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*function, debug, NULL, true);
}

void AMUCameraController::DisplayDialog(int32 index, int32 next)
{
	if (next == 2)
	{
		CloseDialog();
		return;
	}
	const FString command = FString::Printf(TEXT("SetTutorialMessage %d %d"), index, next);
	FOutputDeviceDebug debug;
	GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
	if (SoundComponent != NULL)
	{
		SoundComponent->Stop();
		SoundComponent->Deactivate();
		SoundComponent->DestroyComponent();
	}
	SoundComponent = UGameplayStatics::SpawnSoundAttached(TutorialSoundCue[index], RootComponent);
}

void AMUCameraController::CloseDialog()
{
	const FString command = FString::Printf(TEXT("CloseTutorialMessage"));
	FOutputDeviceDebug debug;
	GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

void AMUCameraController::ProcessDialog()
{
	progression++;
	DisplayDialog(progression, nextList[progression]);
}

void AMUCameraController::UpdateBalance(int32 change)
{
    const FString command = FString::Printf(TEXT("ChangeBudget %d"), change);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

void AMUCameraController::UpdateReputation(int32 change)
{
    const FString command = FString::Printf(TEXT("ChangeReputation %d"), change);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

void AMUCameraController::UpdateMonth(int32 change)
{
    const FString command = FString::Printf(TEXT("SetMonth %d"), change);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

void AMUCameraController::UpdateSemester(int32 change)
{
    const FString command = FString::Printf(TEXT("SetSemester %d"), change);
    FOutputDeviceDebug debug;
    GetHUD()->CallFunctionByNameWithArguments(*command, debug, this, true);
}

float AMUCameraController::MovementSpeedCalculation()
{
	float result = 1.0f;
	if (CameraPawn)
	{
		result = FMath::Clamp<float>(CameraPawn->GetCameraSpring()->TargetArmLength / 300, 1.f, 4.f);
	}
	return result;
}

void AMUCameraController::PanX(float value)
{
    if(bBuildMode)
    {
        return;
    }
	if (bEnablePan)
	{
		bEnableMoveX = false;
		FRotator rotation = CameraPawn->GetActorRotation();
		rotation.Yaw += value * PanSensitivity;
		CameraPawn->SetActorRotation(rotation);
	}
	else
	{
		bEnableMoveX = true;
	}

	if (bEnableMoveX)
	{
		int screenX, screenY;
		GetViewportSize(screenX, screenY);
		float X, Y;
		GetMousePosition(X, Y);
		if (X < 0)
		{
			return;
		}

		float edgeMovementSpeedX = 0.0f;
		if (X / screenX >= 0.96)
		{
			edgeMovementSpeedX = EdgeSensitivity;
		}
		else if (X / screenX <= 0.04)
		{
			edgeMovementSpeedX = -EdgeSensitivity;
		}

		if (edgeMovementSpeedX != 0.0f)
		{
            if(CameraPawn == nullptr)
            {
                return;
            }
			CameraPawn->AddActorLocalOffset(FVector(0.f, edgeMovementSpeedX * MovementSpeedCalculation(), 0.f), true);
			auto offset = CameraPawn->GetActorLocation();
			offset.Z = 350.f;
			CameraPawn->SetActorLocation(offset);
		}
	}
}

void AMUCameraController::PanY(float value)
{
    if(bBuildMode)
    {
        return;
    }
	if (bEnablePan)
	{
		bEnableMoveY = false;
		FRotator rotation = CameraPawn->GetActorRotation();
		rotation.Pitch += value * PanSensitivity;
		rotation.Pitch = FMath::Clamp<float>(rotation.Pitch, -15, 45);
		CameraPawn->SetActorRotation(rotation);
	}
	else
	{
		bEnableMoveY = true;
	}

	if (bEnableMoveY)
	{
		int screenX, screenY;
		GetViewportSize(screenX, screenY);
		float X, Y;
		GetMousePosition(X, Y);
		if (Y < 0)
		{
			return;
		}

		float edgeMovementSpeedY = 0.0f;
		if (Y / screenY >= 0.96)
		{
			edgeMovementSpeedY = -EdgeSensitivity;
		}
		else if (Y / screenY <= 0.04)
		{
			edgeMovementSpeedY = EdgeSensitivity;
		}

		if (edgeMovementSpeedY != 0.0f)
		{
            if(CameraPawn == nullptr)
            {
                return;
            }
			CameraPawn->AddActorLocalOffset(FVector(edgeMovementSpeedY * MovementSpeedCalculation(), 0.f, 0.f), true);
			auto offset= CameraPawn->GetActorLocation();
			offset.Z = 350.f;
			CameraPawn->SetActorLocation(offset);
		}
	}
}

void AMUCameraController::Clicked()
{
	if (bBuildMode)
	{
		myBuildingGhost->Clicked();
	}
}

void AMUCameraController::RightClicked()
{
    if(bBuildMode)
    {
        myBuildingGhost->ClearSpawn();
    }
}

void AMUCameraController::ZoomIn()
{
    if(bBuildMode)
    {
        return;
    }
	float length = CameraPawn->GetCameraSpring()->TargetArmLength - ZoomSensitivity;
	length = FMath::Clamp<float>(length, 300.f, 1200.f);
	CameraPawn->GetCameraSpring()->TargetArmLength = length;
}

void AMUCameraController::ZoomOut()
{
    if(bBuildMode)
    {
        return;
    }
	float length = CameraPawn->GetCameraSpring()->TargetArmLength + ZoomSensitivity;
	length = FMath::Clamp<float>(length, 300.f, 1200.f);
	CameraPawn->GetCameraSpring()->TargetArmLength = length;
}

float AMUCameraController::SetDistance()
{
	return (CameraPawn->GetCameraSpring()->TargetArmLength + SightDistance);
}

FVector AMUCameraController::UpdateMousePosition()
{
	return FVector();
}
