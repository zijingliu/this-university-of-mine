// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class THISUNIVERSITYOFMINE_API MUEvent
{
public:
	MUEvent();
	~MUEvent();
private:
    int type;
    int importance;
    int time;
    int duration;
};
