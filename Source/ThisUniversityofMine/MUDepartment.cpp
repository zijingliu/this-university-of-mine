// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUDepartment.h"
#include "MUStudent.h"
#include <iostream>
#include "MUUniversity.h"
#include "MUCameraController.h"
#include "MUAIManager.h"

MUDepartment::MUDepartment(MUUniversity* university, int32 depType)
{
    academic = 100;
    SATmin = 1000;
    GPAmin = 2.0;
    satisfaction = 80;
    for (int i = 0; i < grades; i++) population[i] = 0;
    maxPopulation = 200;
    instructors = 1;
    reputation = 100;
    mUniveristy = university;
    this->type = depType;
    
    int32 index = type*6 + 6;
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index, (int32)getAcademic());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index+1, (int32)getSatisfation());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index+2, (int32)getPopulation(-1));
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index+3, (int32)getMaxPopulation());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index+4, (int32)getInstructors());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(index+5, (int32)getReputation());
    
    mUniveristy->GetManager()->GetController()->UpdateReputation(type, (int32)getReputation());
}

MUDepartment::~MUDepartment(){
    
}

void MUDepartment::graduate(int32& total){
    for (int i = grades-1; i > 0; i--) {
        population[i] = population[i-1];
    }
    population[0] = newStudents.size();
    newStudents.clear();
}

void MUDepartment::transferOut(int32& total){
    for (int i = 0; i < grades-1; i++) {
        int num = (double)population[i] * abs(rand()%200) * (100.0 - satisfaction) / 50000.0;
        total += num;
        population[i] -= num;
    }
}

void MUDepartment::generateNewStudents(){
    newStudents.clear();
    for (int i = 0; i < (int)reputation; i++) {
        newStudents.push_back(MUStudent());
    }
}

void MUDepartment::processAdmision(int32& total){
    std::vector<MUStudent> temp;
    int aPosition = getAPosition();
    for (auto& student : newStudents){
        int num = temp.size();
        if (num >= aPosition){
            break;
        }
        if (student.GPA > GPAmin && student.SAT > SATmin) {
            temp.push_back(student);
        }
    }
    newStudents.clear();
    newStudents = temp;
    total = newStudents.size();
}

void MUDepartment::updateDepartment(){
    float totalPop = getPopulation(grades);
    float senior = getPopulation(grades-1);
    reputation += senior;
    float acdDec = senior * 1.2 * academic / (totalPop? totalPop : 1);
    for (auto& student : newStudents) {
        academic += student.academicPotential(academic);
    }
    academic -= acdDec;
    reputation -= ((totalPop/instructors) - 30);
    academic -= ((totalPop/instructors) - 30);
    if (reputation < 10) reputation = 10;
    if (academic < 10) academic = 10;
}

void MUDepartment::newSemester(FString& summary){
    int32 transfer = 0;
    int32 freshmen = 0;
    int32 graduation = 0;
    float preReputation = getReputation();
    transferOut(transfer);
    generateNewStudents();
    processAdmision(freshmen);
    updateDepartment();
    graduate(graduation);
    
    int32 reputationChange = (int32)(getReputation() - preReputation);
    // population
    // reputation
    // academic
    
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+8, (int32)getPopulation(-1));
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+6, (int32)getAcademic());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+11, (int32)getReputation());
    mUniveristy->GetManager()->GetController()->UpdateReputation(type, reputationChange);
    
    summary += FString::Printf(TEXT(" %d students transfer out, %d new students come, %d students graduate!"), transfer, freshmen, graduation);
}

void MUDepartment::ExtraAdmission(FString& summary)
{
    int freshmen = 0;
    generateNewStudents();
    processAdmision(freshmen);
    updateDepartment();
    
    summary += FString::Printf(TEXT("%d students just transfer in!"), freshmen);
}

int MUDepartment::getAPosition(){
    int max = maxPopulation - (getPopulation(-1) - getPopulation(grades-1));
    int max2 = mUniveristy->getAPosition();
    return (max < max2) ? max : max2;
}

void MUDepartment::build(){
    maxPopulation += 200;
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+9, (int32)getMaxPopulation());
}

bool MUDepartment::hire(int num){
    if(mUniveristy->hire(num))
    {
        instructors += num;
        mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+10, (int32)getInstructors());
		mUniveristy->GetManager()->GetController()->UpdateBalance(type, (int32)(-num*mUniveristy->getSalary()));
		return true;
    }
	else
	{
		return false;
	}
}

void MUDepartment::SatChange(float change){
    satisfaction *= 1 + change;
    if (satisfaction > 100) satisfaction = 100;
    else if (satisfaction < 0) satisfaction = 1;
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+7, (int32)getSatisfation());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(1, (int32)(mUniveristy->getSatisfaction()));
}

void MUDepartment::repChange(float change){
    float changef = reputation * change;
    reputation += changef;
    if (reputation < 0) reputation = 1;
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+11, (int32)getReputation());
    mUniveristy->GetManager()->GetController()->UpdateReputation(type, (int32)changef);
    mUniveristy->GetManager()->GetController()->UpdateUIDate(5, (int32)(mUniveristy->getReputation()));
    mUniveristy->GetManager()->GetController()->UpdateReputation((int32)(mUniveristy->getReputation()));
}

void MUDepartment::AcdChange(float change){
    academic *= 1 + change;
    if (academic < 0) academic = 1;
    mUniveristy->GetManager()->GetController()->UpdateUIDate(type*6+6, (int32)getAcademic());
    mUniveristy->GetManager()->GetController()->UpdateUIDate(0, (int32)(mUniveristy->getAcademic()));
}