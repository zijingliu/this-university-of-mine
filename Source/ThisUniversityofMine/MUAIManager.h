// Fill out your copyright notice in the Description page of Project Settings.

#include <map>
#include <vector>

#pragma once

#include "GameFramework/Actor.h"
#include "MUBuilding.h"
#include "MUAIManager.generated.h"

class AMUCameraController;
UCLASS()
class THISUNIVERSITYOFMINE_API AMUAIManager : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AMUAIManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    class MUUniversity* getUniversity(){ return mUniversity; }
    
    bool EventBuild(AMUBuilding* building, FVector loc);
    
    void AIToGo(FVector location, float percentage);
    void AIFinish();
    void buildAI();
    
    void SetController(AMUCameraController* controller);
    AMUCameraController* GetController() { return myController; }
    void DisplayMsg(FString text, int32 color);
    void AddAI();
    float GetRemainingTime();
    void StartGame();
    

    constexpr static const float bound = 500.0f;
    
    struct Task{
        Task(int num, FVector Loc){
            numNeeded = num;
            loc = Loc;
        }
        int numNeeded;
        FVector loc;
    };
    
    enum CampusEvents{
        EventNewSemester,
        EventProcessTime,
        EventCelebrateEng,
        EventHealthCenter,
        EventStadium,
        EventResturant,
        EventShop
    };
    
    enum CampusPolicies{
        PolicyTechSchool,
        PolicyPartyScool,
		NoPolicy
    };
    
    void AddEvent(CampusEvents event, float time, bool loop);
    
    // Events list
    void ProcessTime();
    void NewSemester();
    void CelebrateEng();
    void HealthCenter();
    void Stadium();
    void Resturant();
    void Shop();
    
    //Policies list
    void TechSchool();
    void PartySchool();

	// Policy enforcement
	bool ApplyPolicy(CampusPolicies cp);
    
    void destroyAI();
    std::map<int32, FVector> buildingLocations;
    
protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<ACharacter> CharacterClass;
    
private:
    FVector roundLocation(const FVector& loc);
//    void toDoTask(class AMUAIController* controller, Task* task);
    std::map<float, Task*> tasks;
    class MUUniversity* mUniversity;
    UPROPERTY()
    float month;
    UPROPERTY()
    float sems;
    std::map<float,std::vector<class AMUAIController*> > busyAI;
    std::vector<class AMUAIController*> allAIs;
    
    bool eventAlive = false;
    AMUCameraController* myController;
    FTimerHandle timeHandle;
    
    TArray<CampusEvents> EventList;
    
    bool bHealthCenter = false;
    bool bStadium = false;
    bool bResturant = false;
    bool bShop = false;

	CampusPolicies currentPolicy = NoPolicy;
};
