// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "MUCameraController.h"
#include "Blueprint/UserWidget.h"
#include "ThisUniversityofMineGameMode.generated.h"

/**
 * 
 */
UCLASS()
class THISUNIVERSITYOFMINE_API AThisUniversityofMineGameMode : public AGameMode
{
	GENERATED_BODY()
public:
    AThisUniversityofMineGameMode();
	void BeginPlay() override;
	/** Remove the current menu widget and create a new one from the specified class, if provided. */
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	AMUCameraController* GetController();
    
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    UUserWidget* GetMainUI() { return CurrentWidget; }

protected:
	/** The widget class we will use as our menu when the game starts. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Mesh)
	UMaterialInstance* Error;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Mesh)
	UMaterialInstance* Normal;

	/** The widget instance that we are using as our menu. */
	UPROPERTY()
	UUserWidget* CurrentWidget;

	AMUCameraController* controller;
};
	