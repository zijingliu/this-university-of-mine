// Fill out your copyright notice in the Description page of Project Settings.

#include <vector>
#include "MUStudent.h"

#pragma once

/**
 * 
 */
class THISUNIVERSITYOFMINE_API MUDepartment
{
public:
	MUDepartment(class MUUniversity* university, int32 depType);
    ~MUDepartment();
    UPROPERTY(EditAnywhere)
    static const int grades = 4;
    void transferOut(int32& total);
    float getAcademic(){ return academic; }
    float getSatisfation(){ return satisfaction; }
    int getPopulation(int grade){
        int num = 0;
        if (grade < 4 && grade >= 0) return population[grade];
        else {
            for (int i = 0; i < 4; i++) num += population[i];
        }
        return num;
    }
    int getMaxPopulation(){ return maxPopulation; }
    int getInstructors() { return instructors; }
    float getReputation() { return reputation; }
    const std::vector<MUStudent>& getNewStudents(){ return newStudents; }
    void newSemester(FString& summary);
    int getAPosition();
    int SATmin;
    float GPAmin;
    void build();
    bool hire(int num);
    void SatChange(float change);
    void AcdChange(float change);
    void repChange(float change);
    void ExtraAdmission(FString& summary);
    int32 type;

private:
    void generateNewStudents();
    void processAdmision(int32& total);
    void updateDepartment();
    void graduate(int32& total);
    std::vector<MUStudent> newStudents;
    float academic;
    float satisfaction;
    int population[4];
    int maxPopulation;
    int instructors;
    float reputation;
    class MUUniversity* mUniveristy;
};
