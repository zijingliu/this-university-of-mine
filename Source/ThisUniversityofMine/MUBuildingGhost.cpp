// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUBuildingGhost.h"
#include "MUBuilding.h"
#include "MULibrary.h"


// Sets default values
AMUBuildingGhost::AMUBuildingGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    BuildingMeshCompoent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BuildMeshComponent"));
	RootComponent = BuildingMeshCompoent;
    BuildingMesh = nullptr;
}

// Called when the game starts or when spawned
void AMUBuildingGhost::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMUBuildingGhost::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (IsActive)
	{
		auto pos = SetGridSnap();
		SetActorLocation(pos);
		IsOverlapping = CheckOverlap();
		if (IsOverlapping)
		{
			BuildingMeshCompoent->SetMaterial(0, Error);
		}
		else
		{
			BuildingMeshCompoent->SetMaterial(0, Normal);
		}
	}

}

void AMUBuildingGhost::RotateGhost(float sign)
{
    auto rot = GetActorRotation();
    rot.Add(0, sign*45.f, 0);
    SetActorRotation(rot);
}

void AMUBuildingGhost::SetController(AMUCameraController *parent)
{
    if(parent)
    {
        controller = parent;
    }
}

void AMUBuildingGhost::OnSpawn(UStaticMesh* StaticMesh)
{
    if(!IsActive)
    {
        BuildingMesh = StaticMesh;
        IsActive = true; 
		controller->SetBuildMode(true);
        BuildingMeshCompoent->SetStaticMesh(BuildingMesh);
		BuildingMeshCompoent->SetMaterial(0, Normal);
		BuildingMeshCompoent->SetVisibility(true);
    }
}

void AMUBuildingGhost::ClearSpawn()
{
    IsActive = false;
	controller->SetBuildMode(false);
	IsOverlapping = false;
	BuildingMeshCompoent->SetVisibility(false);
	CurrentBuilding = AMUBuilding::StaticClass();
    if(Building)
    {
        Building->K2_DestroyActor();
    }
}

void AMUBuildingGhost::EventBuild(TSubclassOf<class AMUBuilding> MainBuilding)
{
    Building = GetWorld()->SpawnActor<AMUBuilding>(MainBuilding, FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f));
	Building->SetController(controller);
	if (Building)
	{
		ClearSpawn();
		CurrentBuilding = MainBuilding;
		BuildingMeshCompoent->SetWorldScale3D(Building->GetBuildingScale());
		OnSpawn(Building->GetBuildingMesh());
        Building->GetStaticMeshComp()->SetVisibility(false);
	}
}

void AMUBuildingGhost::SetMousePosition(FVector pos)
{
	MousePosition = pos;
}

void AMUBuildingGhost::Clicked()
{
	if (!IsOverlapping && controller->EventBuild(Building))
	{
		auto actualBuilding = GetWorld()->SpawnActor<AMUBuilding>(CurrentBuilding, GetActorLocation(), GetActorRotation());
		ClearSpawn();
	}
}

void AMUBuildingGhost::SetNormalMaterial(UMaterialInstance * n)
{
	Normal = n;
}

void AMUBuildingGhost::SetErrorMaterial(UMaterialInstance * e)
{
	Error = e;
}

FVector AMUBuildingGhost::SetGridSnap()
{
	return FVector(UMULibrary::SetGridSnap(MousePosition.X, GridSize), UMULibrary::SetGridSnap(MousePosition.Y, GridSize), UMULibrary::SetGridSnap(MousePosition.Z, GridSize));
}

bool AMUBuildingGhost::CheckOverlap()
{
	TArray<AActor*> OverlappingResult;
	GetOverlappingActors(OverlappingResult, AActor::StaticClass());
	if (OverlappingResult.Num() > 0)
	{
		return true;
	}
	return false;
}

