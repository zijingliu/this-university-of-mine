// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class THISUNIVERSITYOFMINE_API MUStudent
{
public:
	MUStudent();
	~MUStudent();
    float GPA;
    float SAT;
    float academicPotential(float academic); // GPA * SAT * rand(0.5 ~ 2.0) * academic
};
