// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUPawnWithCamera.h"
#include "MUCameraController.h"

// Sets default values
AMUPawnWithCamera::AMUPawnWithCamera()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
    //Create our components
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    OurCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
    OurCameraSpringArm->AttachTo(RootComponent);
    OurCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.f), FRotator(-70.f, 0.f, 0.0f));
    OurCameraSpringArm->TargetArmLength = 600.f;
    OurCameraSpringArm->bEnableCameraLag = false;
    
    OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
    OurCamera->AttachTo(OurCameraSpringArm, USpringArmComponent::SocketName);
	SetActorLocation(FVector(0.f, 0.f, 350.f));
	//SetActorRotation(FVector(0.0f, 0.0f, -90.0f));
	SetActorRotation(FRotator(0.0f, -90.0f, 0.0f));
}