// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUAIManager.h"
#include "MUAICharacter.h"
#include "MUUniversity.h"
#include "MUAIController.h"
#include <iostream>

// Sets default values
AMUAIManager::AMUAIManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    month = 1;
    sems = 1;
}

// Called when the game starts or when spawned
void AMUAIManager::BeginPlay()
{
	Super::BeginPlay();
    mUniversity = new MUUniversity();
    mUniversity->SetAIManger(this);
    allAIs.clear();
    mUniversity->InitilizeUniversity();
}

// Called every frame
void AMUAIManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
    for (auto& AI : allAIs)
    {
        if (!AI->isBusy())
        {
            if (AI)
            {
                AI->goTo(mUniversity->getRandLoc());
            }
        }
    }
}

void AMUAIManager::StartGame()
{
  AddEvent(EventProcessTime, 10.f, true); 
  myController->DisplayDialog(0, 1);
}

void AMUAIManager::AddAI()
{
    int num = mUniversity->getPopulation(-1) / 100;
	if ((mUniversity->getPopulation(-1) % 100) == 0) return;
    if(num < 1) num = 1;
    while(allAIs.size() < num)
    {
        buildAI();
    }
}

void AMUAIManager::DisplayMsg(FString text, int32 color)
{
    if(myController != NULL)
    {
        myController->DisplayInformation(text, color);
    }
}

void AMUAIManager::SetController(AMUCameraController *controller)
{
    if(controller)
    {
        myController = controller;
    }
}

void AMUAIManager::HealthCenter()
{
    mUniversity->SatChange(0.05f);
    mUniversity->ChangeBalance(-100000.f);
    myController->DisplayInformation(FString(TEXT("Health Center costs 100k but increase 5% satisfication for all school!")), 2);
    AIToGo(buildingLocations[7], 1);
}

void AMUAIManager::Stadium()
{
    int r = rand() % 2;
    if(r == 0)
    {
        mUniversity->SatChange(0.1);
        mUniversity->ChangeBalance(+250000.0f);
        myController->DisplayInformation(FString(TEXT("We are the champion of the NFL this semster!!")), 2);
        
    }
    else
    {
        mUniversity->SatChange(-0.1);
        mUniversity->ChangeBalance(-250000.0f);
        myController->DisplayInformation(FString(TEXT("We lost all of the games this semester. students are pissed off!")), 2);
    }
    
}


void AMUAIManager::Resturant()
{
    mUniversity->SatChange(-0.1f);
    mUniversity->ChangeBalance(100000.f);
    myController->DisplayInformation(FString(TEXT("Cafe84 produces 100k profits but students hate it!")), 2);
}

void AMUAIManager::Shop()
{
    mUniversity->ChangeBalance(50000.f);
    myController->DisplayInformation(FString(TEXT("The small market gains 50k profits")), 2);
}

void AMUAIManager::AddEvent(AMUAIManager::CampusEvents event, float time, bool loop)
{
    FTimerHandle handle;
    if(event == EventProcessTime)
    {
        GetWorldTimerManager().SetTimer(timeHandle, this, &AMUAIManager::ProcessTime, time, loop);
    }
    else if(event == EventCelebrateEng)
    {
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::CelebrateEng, time, loop);
    }
    else if(event == EventHealthCenter)
    {
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::HealthCenter, time, loop);
    }
    else if(event == EventStadium)
    {
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::Stadium, time, loop);
    }
    else if(event == EventResturant)
    {
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::Resturant, time, loop);
    }
    else if(event == EventShop)
    {
        GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::Shop, time, loop);
    }
}

float AMUAIManager::GetRemainingTime()
{
    return GetWorldTimerManager().GetTimerRemaining(timeHandle);
}

void AMUAIManager::CelebrateEng()
{
    mUniversity->SatChange(0.1f);
    myController->DisplayInformation(FString(TEXT("Students are happy about having a new engineering building! Satification level is improved by 10%! Fight on!")), 1);
    
    FVector loc = buildingLocations[1];
    AIToGo(loc, 1);
    
//    if(buildingLocations.find(1) != buildingLocations.end())
//    {
//         AIToGo(1.f, buildingLocations[1], 1.f);
//    }
//    else
//    {
//        UE_LOG(YourLog,Warning,TEXT("Not found enginner!"));
//    }
}

void AMUAIManager::ProcessTime()
{
    month++;
    if(month == 5)
    {
        NewSemester();
        month = 1;
        sems++;
        for(CampusEvents ce : EventList)
        {
            AddEvent(ce, 0.0f, false);
        }

		if (currentPolicy == PolicyTechSchool)
		{
			TechSchool();
		}
		else if (currentPolicy == PolicyPartyScool)
		{
			PartySchool();
		}

        myController->UpdateSemester((int32)sems);
    }
    myController->UpdateMonth((int32)month);
}

void AMUAIManager::NewSemester()
{
    mUniversity->newSemester();
}

bool AMUAIManager::EventBuild(AMUBuilding* building, FVector loc)
{
    if(mUniversity->build(building->GetType(), loc, building->GetCost(), building->GetSatChange(), building->GetAcademicChange(), building->GetReputationChange()))
    {
        if(buildingLocations.find(building->GetType()) == buildingLocations.end())
        {
            buildingLocations[building->GetType()] = loc;
        }
        else if(building->GetType() == MUUniversity::HealthCenter && !bHealthCenter)
        {
            EventList.Emplace(EventHealthCenter);
            bHealthCenter = true;
        }
        else if(building->GetType() == MUUniversity::Stadium && !bStadium)
        {
            EventList.Emplace(EventStadium);
            bStadium = true;
        }
        else if(building->GetType() == MUUniversity::Dinning && !bResturant)
        {
            EventList.Emplace(EventResturant);
            bResturant = true;
        }
        else if(building->GetType() == MUUniversity::Shop && !bShop)
        {
            EventList.Emplace(EventShop);
            bShop = true;
        }   
        
        return true;
    }
    return false;
}

FVector AMUAIManager::roundLocation(const FVector& loc){
    return FVector(10*cosf(rand()) + loc.X, 10*sinf(rand()) + loc.Y, loc.Z);
}

void AMUAIManager::AIToGo(FVector location, float percentage){
    std::cout << "AIMove " << location.X << " " << location.Y << std::endl;
    int num = percentage * allAIs.size();
    for (auto& ai : allAIs) {
        if (!ai->inEvent()) {
            ai->goTo(roundLocation(location), true);
        }
    }
    FTimerHandle handle;
    GetWorldTimerManager().SetTimer(handle, this, &AMUAIManager::AIFinish, 5.0f, false);
    
//    busyAI[eventID] = ais;
//    if (num > ais.size()) {
//        tasks[eventID] = new Task(num - ais.size(), location);
//    }
}

void AMUAIManager::AIFinish(){
//    auto& ais = busyAI[eventID];
    auto& ais = allAIs;
    for (auto& ai : ais) {
        if (ai){
            ai->allowAutoControl();
            ai->endEvent();
        }
    }
//    busyAI.erase(busyAI.find(eventID));
//    if (tasks.find(eventID) != tasks.end()) {
//        delete tasks[eventID];
//        tasks.erase(tasks.find(eventID));
//    }
}

void AMUAIManager::buildAI(){
    int x = FMath::RandRange(-1 * bound, bound);
    int y = FMath::RandRange(-1 * bound, bound);
    ACharacter* Char = GetWorld()->SpawnActor<AMUAICharacter>(CharacterClass);
    if(Char)
    {
        Char->SetActorLocation(FVector(x, y, 0));
    }
    else return;
    if (Char) Char->SpawnDefaultController();
    allAIs.push_back(Cast<AMUAIController>(Char->GetController()));
}

bool AMUAIManager::ApplyPolicy(CampusPolicies cp)
{
	if (mUniversity->CheckBalance(1000000.0f))
	{
		currentPolicy = cp;
		mUniversity->ChangeBalance(-1000000.0f);
		return true;
	}
	else
	{
		return false;
	}
}

void AMUAIManager::destroyAI(){
    auto ai = allAIs[allAIs.size()-1];
    allAIs.pop_back();
    auto pawn = ai->GetPawn();
    pawn->Destroyed();
    ai->Destroy();
}


void AMUAIManager::TechSchool()
{
	mUniversity->TechPolicy();
	myController->DisplayInformation(FString(TEXT("The university is applying tech school policy!")), 2);
}

void AMUAIManager::PartySchool()
{
	mUniversity->PartyPolicy();
	myController->DisplayInformation(FString(TEXT("The university is applying party school policy!")), 2);
}