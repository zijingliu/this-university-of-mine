// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUAICharacter.h"
#include "MUAIController.h"
#include <iostream>

// Sets default values
AMUAICharacter::AMUAICharacter()
{
 	// Set this character to call uTick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    AIControllerClass = AMUAIController::StaticClass();
}

// Called when the game starts or when spawned
void AMUAICharacter::BeginPlay()
{
    Super::BeginPlay();
//    GetMovementComponent()
}

// Called every frame
void AMUAICharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    auto controller = Cast<AMUAIController>(GetController());
    auto dist = FVector::Dist(GetActorLocation(), controller->getDest());
}

// Called to bind functionality to input
void AMUAICharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

//void AMUAICharacter::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit){
//    auto controller = Cast<AMUAIController>(GetController());
//    auto dist = FVector::Dist(controller->getDest(), GetActorLocation());
////    std::cout << "dist " << dist << std::endl;
//    if (dist < 140.0f) {
////        std::cout << "arrive " << std::endl;
//        controller->arriveDest();
//    }
//}

void AMUAICharacter::NotifyActorBeginOverlap(AActor* OtherActor){
    auto controller = Cast<AMUAIController>(GetController());
    auto dist = FVector::Dist(controller->getDest(), GetActorLocation());
//    std::cout << "dist " << dist << std::endl;
    if (dist < 140.0f) {
//        std::cout << "arrive " << std::endl;
        controller->arriveDest();
    }
}