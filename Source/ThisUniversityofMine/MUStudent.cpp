// Fill out your copyright notice in the Description page of Project Settings.

#include "ThisUniversityofMine.h"
#include "MUStudent.h"

MUStudent::MUStudent()
{
    GPA = (float)(abs(rand())%200) / 100.0 + 2.0;
    SAT = (abs(rand())%141)*10 + 1000;
}

MUStudent::~MUStudent()
{
}

float MUStudent::academicPotential(float academic){
    return GPA * SAT * ((float)(abs(rand())%300)/200.0+0.5)
    * academic / (4.0 * 2400.0 * 100);
}