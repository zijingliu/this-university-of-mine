# This University of Mine #

* Zijing Liu, Zhenyang Zhong, Zheman Shi
* ITP-380 final project
* This is a finished Unreal 4 project, not a built game.

A simulation game that you play as the president of your university. How do you want to build the campus? How do you want to develop different departments? How you want to attract more students to your university and pursue their education goals?